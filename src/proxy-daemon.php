<?php

use App\Classes\Proxy;

require_once 'bootstrap.php';
// Fork settings
pcntl_async_signals(true);

pcntl_signal(SIGTERM, 'signalHandler'); // Termination ('kill' was called)
pcntl_signal(SIGHUP, 'signalHandler'); // Terminal log-out
pcntl_signal(SIGINT, 'signalHandler'); // Interrupted (Ctrl-C is pressed)

Proxy::load();

function signalHandler()
{
    foreach (Proxy::$pid as $pid) {
        posix_kill($pid, SIGTERM);
    }
    exit;
}
