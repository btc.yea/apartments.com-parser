<?php

// require_once __DIR__.'/bootstrap.php';

// //Fork settings
// pcntl_async_signals(true);

// pcntl_signal(SIGTERM, 'signalHandler'); // Termination ('kill' was called)
// pcntl_signal(SIGHUP, 'signalHandler'); // Terminal log-out
// pcntl_signal(SIGINT, 'signalHandler'); // Interrupted (Ctrl-C is pressed)

// save parent pid
file_put_contents('parentPid.out', getmypid());


if (isset($argv[1]) && 'init' === $argv[1]) {
    echo "Init..".PHP_EOL;

    $redis = \App\Classes\Redis::init();
    $redis->flushall();

    $citiesDB = file_get_contents(__DIR__.'/cities.json');
    $citiesDB = json_decode($citiesDB, true);


    /**
     * paste first links in queue
     * and now we can get links for parsing...
     */
    foreach ($citiesDB as $states) {
        foreach ($states as $code => $citiesArray) {
            foreach ($citiesArray as $city) {
                $city = str_replace(' ', '-',strtolower($city));
                $code = strtolower($code);
                $base_link = 'https://www.apartments.com/'.urlencode($city.'-'.$code).'/';
                echo $base_link.PHP_EOL;
                $task = '{"link":"'.$base_link.'", "method":"get_links"}';
                $redis->rpush('tasks', $task);
            }
        }
    }
} elseif (isset($argv[1]) && 'deletions' === $argv[1]) {
    $db = new \App\Classes\MySQL;
    $redis = \App\Classes\Redis::init();

    $date = date('Y-m-d');
    $date = date('Y-m-d', strtotime($date. ' + '.env('DATE_PLUS_DAYS', 3).' days'));

    $query = $db->pdo->prepare("SELECT `link` FROM `properties` WHERE `is_deleted` = ? AND DATE(`last_update`) >= ?");
    $query->execute(['0', $date]);
    $links = $query->fetchAll();
    foreach ($links as $link) {
        $task = '{"link":"'.$link->link.'", "method":"update"}';
        $redis->rpush('tasks', $task);
    }
} elseif (isset($argv[1]) && 'byZip' === $argv[1]) {
    echo "Init..".PHP_EOL;

    // $redis = \App\Classes\Redis::init();
    // $redis->flushall();

    $citiesDB = file_get_contents(__DIR__.'/cities.json');
    $citiesDB = json_decode($citiesDB, true);
    $zipDB = file_get_contents(__DIR__.'/zipcodes.json');
    $zipDB = json_decode($zipDB, true);


    /**
     * paste first links in queue
     * and now we can get links for parsing...
     */
    foreach ($citiesDB as $states) {
        foreach ($states as $code => $citiesArray) {
            foreach ($citiesArray as $city) {
                foreach ($zipDB as $key => $zip) {
                    $city = str_replace(' ', '-',strtolower($city));
                    $code = strtolower($code);
                    $base_link = 'https://www.apartments.com/'.urlencode($city.'-'.$code.'-'.$zip).'/';
                echo $base_link.PHP_EOL;

                    // $task = '{"link":"'.$base_link.'", "method":"get_links"}';
                    // $redis->rpush('tasks', $task);
                }
            }
        }
    }
}

echo "Parse..".PHP_EOL;

$queue = new \App\Classes\Queue();
$queue->start();

function signalHandler($signal)
{
    global $queue;
    unset($queue);
    exit;
}
