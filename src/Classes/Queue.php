<?php

namespace App\Classes;

use DiDom\Document;

class Queue
{
    protected $pid = [];
    protected bool $parent = true;
    protected $limit;
    protected $stop = false;

    public function __construct()
    {
        $this->limit = env('FORK_LIMIT', 100);
    }

    public function __destruct()
    {
        if (true === $this->parent) {
            foreach ($this->pid as $pid => $link) {
                posix_kill($pid, SIGTERM);
                Redis::init()->rpush('aborted_links', $link);
            }
        }
    }

    public function start()
    {
        while (!$this->stop) {
            $this->checkPid();
            $task = json_decode(Redis::init()->brpop('tasks'), true);

            if (count($this->pid) < $this->limit && $task) {
                // get link
                $link = $task['link'];
                $method = $task['method'];
                $this->fork($link, $method);
            } elseif (count($this->pid) >= $this->limit && $task) {
                Redis::init()->rpush('tasks', json_encode($task));
                sleep(1);
            } elseif (!$task) {
                // we does not have tasks, and have free proccess, so parsing completed 
                echo "COMPLETED!" . PHP_EOL;
                $this->stop = true;
            }
        }
    }

    protected function checkPid()
    {
        foreach (array_keys($this->pid) as $pid) {
            $res = pcntl_waitpid($pid, $status, WNOHANG);

            // If the process has already exited
            if (-1 === $res || $res > 0) {
                unset($this->pid[$pid]);
            }
        }
    }

    protected function fork($link, $method = 'parse')
    {
        $pid = pcntl_fork();

        if (-1 === $pid) {
            die('Could not fork');
        }

        // if fork is ok then do next
        if ($pid) {
            $this->pid[$pid] = $link;
        } else {
            $this->parent = false;
            $this->parse($link, $method);
            exit();
        }
    }

    protected function parse($link, $method = 'parse')
    {
        $request = StormProxy::send($link);
        $task = '{"link":"'.$link.'", "method":"'.$method.'"}';

        if (200 === $request['http_code']) {
            try {
                // parse data
                if (is_string($request['response'])) {
                    $content = new Document($request['response']);
                    $crawler = new DataCrawler();
                    $crawler->parse($content, $link, $method);
                } else {
                    Redis::init()->rpush('tasks', $task);
                }
            } catch (\Exception $e) {
                file_put_contents(LOG_DIR.'/parse-problem.log', '['.date('Y-m-d H:i:s').'] Msg:'.$e->getMessage()."\nCode:".$request['http_code'].' - '.$link.PHP_EOL, FILE_APPEND);
            }

            return true;
        } elseif (404 !== $request['http_code']) {
            // back link if not 404

            Redis::init()->rpush('tasks', $task);

            return false;
        }

        if ($method === 'update') {
            $db = new MySQL;
            $date = date('Y-m-d');
            $query = $db->pdo->prepare("UPDATE `properties` SET `last_update` = ?, is_deleted = ? WHERE `link` = ?");
            $query->execute([$date, 1, $link]);
        } else {
            file_put_contents(LOG_DIR.'/404links.log', '['.date('Y-m-d H:i:s').'] '.$link.' method: '.$method.PHP_EOL, FILE_APPEND);
        }
        return false;
    }
}
