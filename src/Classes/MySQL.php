<?php

namespace App\Classes;

class MySQL
{
    public $pdo;

    public function __construct()
    {
        try {
            $pdo = new \PDO(
                "mysql:host=".env('MYSQL_HOST', '127.0.0.1').";dbname=".env('MYSQL_DBNAME', 'jove').";charset=utf8",
                env('MYSQL_USER', 'root'), env('MYSQL_PASSWORD', '')
            );
            $pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_OBJ);
            $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $pdo->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
        }
        catch (PDOException $e) {
            die($e->getMessage());
        }

        $this->pdo = $pdo;
    }

    public function __destruct()
    {
        $this->pdo = null;
    }
}
