<?php

namespace App\Classes;

use Exception;
use JsonException;
use RuntimeException;

class DataCrawler
{
    public function parse($document, $uri, $method)
    {
        if ($document != '') {
            if ($method === 'get_links') {
                $data = $document->find('article');

                foreach ($data as $article) {
                    $link = $article->attr('data-url');
                    if ($link) {
                        $task = '{"link":"'.$link.'", "method":"parse"}';
                        Redis::init()->rpush('tasks', $task);
                    }
                }

                // navigate by pagination
                $pageNumber = explode('/', trim($uri, '/'));
                $pageNumber = is_numeric(end($pageNumber)) ? end($pageNumber) : 1;
                if ($pageNumber === 1) {
                    $pageCount = isset($document->find('span.pageRange')[0]) ? end(explode(' ', trim($document->find('span.pageRange')[0]->text()))) : 1;
                    if ($pageCount > 1) {
                        for ($i=2; $i <= $pageCount; $i++) { 
                            $link = $uri.$i.'/';
                            $task = '{"link":"'.$link.'", "method":"get_links"}';
                            Redis::init()->rpush('tasks', $task);
                        }
                    }
                }
            } elseif ($method === 'parse') {
                $db = new MySQL;
                $query = $db->pdo->prepare("SELECT count(*) FROM `properties` WHERE `link` = ? LIMIT 1");
                $query->execute([$uri]);
                $isDuplicate = $query->fetchColumn();
                if (!$isDuplicate) {
                    // building name
                    $buildingName = isset($document->find('h1.propertyName')[0]) ? trim($document->find('h1.propertyName')[0]->text()) : '';
                    // contact phone
                    $contactPhone = isset($document->find('div.phoneNumber')[0]) ? trim($document->find('div.phoneNumber')[0]->text()) : '';
                    // latitude & longtitude
                    $latitude = isset($document->xpath('//meta[@property="place:location:latitude"]/@content')[0]) ? trim($document->xpath('//meta[@property="place:location:latitude"]/@content')[0]) : '';
                    $longitude = isset($document->xpath('//meta[@property="place:location:longitude"]/@content')[0]) ? trim($document->xpath('//meta[@property="place:location:longitude"]/@content')[0]) : '';
                    // last updated
                    $lastUpdated = isset($document->find('div.lastUpdated')[0]) ? trim($document->find('div.lastUpdated')[0]->text()) : '';

                    // bellow we parse apartment amenities
                    // pet policy
                    $_petPolicy = $document->find('div.petPolicyDetails') ?? [];
                    $petPolicy = '';
                    foreach ($_petPolicy as $pp) {
                        $petPolicy .= $pp->text();
                    }
                    $petPolicy = $this->clearText($petPolicy);
                    // property info
                    $propertyInfo = isset($document->find('div.propertyFeatures ul')[0]) ? 
                        $this->clearText($document->find('div.propertyFeatures ul')[0]->text()) : '';
                    // lease length
                    $leaseLength = isset($document->xpath("//h3[contains(text(), 'Lease Length')]/parent::div/ul")[0]) ? 
                        $this->clearText($document->xpath("//h3[contains(text(), 'Lease Length')]/parent::div/ul")[0]->text()) : '';
                    // outdoor space
                    $outdoorSpace = isset($document->xpath("//h3[contains(text(), 'Outdoor Space')]/parent::div/ul")[0]) ? 
                        $this->clearText($document->xpath("//h3[contains(text(), 'Outdoor Space')]/parent::div/ul")[0]->text()) : '';
                    // on premise services
                    $services = isset($document->xpath("//h3[contains(text(), 'Services')]/parent::div/ul")[0]) ? 
                        $this->clearText($document->xpath("//h3[contains(text(), 'Services')]/parent::div/ul")[0]->text()) : '';
                    // student features
                    $studentFeatures = isset($document->xpath("//h3[contains(text(), 'Student Features')]/parent::div/ul")[0]) ? 
                        $this->clearText($document->xpath("//h3[contains(text(), 'Student Features')]/parent::div/ul")[0]->text()) : '';
                    // on premise features
                    $features = isset($document->xpath("//h3[text()='Features']/parent::div/ul")[0]) ? 
                        $this->clearText($document->xpath("//h3[text()='Features']/parent::div/ul")[0]->text()) : '';
                    // living space
                    $livingSpace = isset($document->xpath("//h3[contains(text(), 'Living Space')]/parent::div/ul")[0]) ? 
                        $this->clearText($document->xpath("//h3[contains(text(), 'Living Space')]/parent::div/ul")[0]->text()) : '';
                    // kitchen
                    $kitchen = isset($document->xpath("//h3[contains(text(), 'Kitchen')]/parent::div/ul")[0]) ? 
                        $this->clearText($document->xpath("//h3[contains(text(), 'Kitchen')]/parent::div/ul")[0]->text()) : '';
                    // security
                    $security = isset($document->xpath("//h3[contains(text(), 'Security')]/parent::div/ul")[0]) ? 
                        $this->clearText($document->xpath("//h3[contains(text(), 'Security')]/parent::div/ul")[0]->text()) : '';
                    // neighborhood comments
                    $neighborhoodComments = isset($document->find('p.clamp')[0]) ? 
                        $this->clearText($document->find('p.clamp')[0]->text()) : '';
                    
                    // contact person
                    $contactPerson = isset($document->find('div.agentFullName')[0]) ? trim($document->find('div.agentFullName')[0]->text()) : '';
                    // building description
                    $buildingDesc = isset($document->find('section.descriptionSection')[0]) ? 
                        $this->clearText($document->find('section.descriptionSection')[0]->text()) : '';
                    // walk score
                    $walkScore = isset($document->find('div.walkScore span.score')[0]) ? 
                        $this->clearText($document->find('div.walkScore span.score')[0]->text()) : '';
                    // transit score
                    $transitScore = isset($document->find('div.transitScore span.score')[0]) ? 
                        $this->clearText($document->find('div.transitScore span.score')[0]->text()) : '';
                    // address
                    $address = isset($document->find('div.propertyAddress h2')[0]) ? 
                        preg_replace('/\s\s+/', ' ', $this->clearText($document->find('div.propertyAddress h2')[0]->text())) : '';
                    $addr1 = isset($document->find('h1.propertyName')[0]) ? 
                        $this->clearText($document->find('h1.propertyName')[0]->text()) : '';
                    $addr2 = isset($document->find('h1.propertyName span')[0]) ? 
                        $this->clearText($document->find('h1.propertyName span')[0]->text()) : '';
                    // parse address
                    $addrArr = isset($document->find('div.propertyAddress h2')[0]) ? $document->find('div.propertyAddress h2')[0]->find('span') : [];
                    
                    // zip5 code, state, city
                    if (count($addrArr) >= 3) {
                        $zip5Code = $this->clearText(end($addrArr)->text());
                        $state = $this->clearText($addrArr[count($addrArr)-2]->text());
                        $city = $this->clearText($addrArr[count($addrArr)-3]->text());
                    } else {
                        $zip5Code = '';
                        $state = '';
                        $city = '';
                    }

                    $availabilityTable = $document->find('tr.rentalGridRow');
                    $availability = [];

                    for ($i = 0; $i < count($availabilityTable); $i++) {
                        $tr = $availabilityTable[$i];
                        $bedroomCnt = isset($tr->find('td.beds span.longText')[0]) ? 
                            $this->clearText($tr->find('td.beds span.longText')[0]->text()) : '';
                        // bedroom count
                        $bathroomCnt = isset($tr->find('td.baths span.longText')[0]) ? 
                            $this->clearText($tr->find('td.baths span.longText')[0]->text()) : '';
                        // listing price
                        $listingPrice = isset($tr->find('td.rent')[0]) ? 
                            $this->clearText($tr->find('td.rent')[0]->text()) : '';
                        // home size sq ft
                        $sqft = isset($tr->find('td.sqft')[0]) ? 
                            $this->clearText($tr->find('td.sqft')[0]->text()) : '';
                        // lease length
                        $leaseLengthAvailability = isset($tr->find('td.leaseLength')[0]) ? 
                            $this->clearText($tr->find('td.leaseLength')[0]->text()) : '';
                        // status
                        $status = isset($tr->find('td.available')[0]) ? 
                            $this->clearText($tr->find('td.available')[0]->text()) : '';

                        array_push($availability, [
                            'bedroom_cnt' => $bedroomCnt,
                            'bathroom_cnt' => $bathroomCnt,
                            'listing_price' => $listingPrice,
                            'home_size_sq_ft' => $sqft,
                            'lease_length' => $leaseLengthAvailability,
                            'status' => $status,
                        ]);
                    }

                    // paste image urls
                    $key = end(explode('/', rtrim($uri, '/')));
                    $imageCrawler = new ImageCrawler;
                    $imagesArr = $imageCrawler->send($key);
                    $imagesLink = [];

                    foreach ($imagesArr as $image) {
                    $link = $image['Uri'];
                    $imagesLink[] = $link;
                    }
                    $imagesLink = !empty($imagesLink) ? json_encode($imagesLink, JSON_PRETTY_PRINT) : '';
                    // nearby colleges
                    $elem = $document->xpath("//th[contains(text(), 'Colleges')]/ancestor::table/tbody/tr") ?? [];
                    $timeType = $document->xpath("//th[contains(text(), 'Colleges')]/ancestor::table/thead")[0] ?? '';
                    $timeType = $timeType != '' ? $timeType->find('th')[1]->text() : '';
                    $nearbyColleges = $this->getNearbyData($elem, $timeType);
                    // nearby transit
                    $elem = $document->xpath("//th[contains(text(), 'Transit / Subway')]/ancestor::table/tbody/tr") ?? [];
                    $timeType = $document->xpath("//th[contains(text(), 'Transit / Subway')]/ancestor::table/thead")[0] ?? '';
                    $timeType = $timeType != '' ? $timeType->find('th')[1]->text() : '';
                    $nearbyTransit = $this->getNearbyData($elem, $timeType);
                    // nearby rail
                    $elem = $document->xpath("//th[contains(text(), 'Commuter Rail')]/ancestor::table/tbody/tr") ?? [];
                    $timeType = $document->xpath("//th[contains(text(), 'Commuter Rail')]/ancestor::table/thead")[0] ?? '';
                    $timeType = $timeType != '' ? $timeType->find('th')[1]->text() : '';
                    $nearbyRail = $this->getNearbyData($elem, $timeType);
                    // nearby shopping
                    $elem = $document->xpath("//th[contains(text(), 'Shopping Centers')]/ancestor::table/tbody/tr") ?? [];
                    $timeType = $document->xpath("//th[contains(text(), 'Shopping Centers')]/ancestor::table/thead")[0] ?? '';
                    $timeType = $timeType != '' ? $timeType->find('th')[1]->text() : '';
                    $nearbyShopping = $this->getNearbyData($elem, $timeType);
                    // nearby parks
                    $elem = $document->xpath("//th[contains(text(), 'Parks and Recreation')]/ancestor::table/tbody/tr") ?? [];
                    $timeType = $document->xpath("//th[contains(text(), 'Parks and Recreation')]/ancestor::table/thead")[0] ?? '';
                    $timeType = $timeType != '' ? $timeType->find('th')[1]->text() : '';
                    $nearbyParks = $this->getNearbyData($elem, $timeType);
                    // nearby airports
                    $elem = $document->xpath("//th[contains(text(), 'Airports')]/ancestor::table/tbody/tr") ?? [];
                    $timeType = $document->xpath("//th[contains(text(), 'Airports')]/ancestor::table/thead")[0] ?? '';
                    $timeType = $timeType != '' ? $timeType->find('th')[1]->text() : '';
                    $nearbyAirports = $this->getNearbyData($elem, $timeType);
                    // utilities included
                    $utilsIncluded = isset($document->find('div.freeUtilities')[0]) ? 
                        $this->clearText($document->find('div.freeUtilities')[0]->find('.descriptionWrapper span')[0]->text()) : '';
                    

                    // expences
                    $expencesArr = isset($document->find('div.oneTimeFees')[0]) ? $document->find('div.oneTimeFees')[0]->find('.descriptionWrapper') : [];
                    $expences = [];
                    foreach ($expencesArr as $ex) {
                        $ex = $ex->find('span');
                        $expence = $ex[0]->text() ?? '';
                        $cost = $ex[1]->text() ?? '';
                        array_push($expences, [
                            'expence' => $expence,
                            'cost' => $cost,
                        ]);
                    }
                    $expences = !empty($expences) ? json_encode($expences, JSON_PRETTY_PRINT) : '';

                    // 3d tours
                    $threeDCrawler = new ThreeDCrawler;
                    $tours = $threeDCrawler->send($key);
                    
                    // office hours
                    $officeHoursArr = isset($document->find('div.officeHours')[0]) ? 
                        $document->find('div.officeHours')[0]->find('div.daysHoursContainer') : '';
                    $officeHours = [];
                    foreach ($officeHoursArr as $oh) {
                        $days = $this->clearText($oh->first('.days')->text());
                        $hours = $this->clearText($oh->first('.hours')->text());
                        $officeHours[] = [
                            'days' => $days,
                            'hours' => $hours
                        ];
                    }
                    $officeHours = !empty($officeHours) ? json_encode($officeHours, JSON_PRETTY_PRINT) : '';

                    // type
                    $type = isset($document->find('span.crumb')[0]) ? $document->find('span.crumb')[0]->first('a')->attr('data-type') : '';
                    $type = ucfirst(substr_replace($type, "", -1));
                    $type = $type == 'Condo' ? 'Apartment' : $type;

                    $date = date('Y-m-d');

                    // paste properties
                    $query = $db->pdo->prepare("INSERT INTO `properties` (
                        `address`,
                        `type`,
                        `addr_line_1`,
                        `addr_line_2`,
                        `building_name`,
                        `contact_phone`,
                        `latitude`,
                        `longitude`,
                        `listing_last_updated`,
                        `pet_policy`,
                        `property_info`,
                        `outdoor_space`,
                        `on_premise_services`,
                        `student_features`,
                        `on_premise_features`,
                        `living_space`,
                        `kitchen`,
                        `building_security`,
                        `contact_person`,
                        `building_desc`,
                        `walk_score`,
                        `transit_score`,
                        `lease_length`,
                        `link`,
                        `city`,
                        `zip5_cd`,
                        `state_cd`,
                        `image_urls`,
                        `nearby_colleges`,
                        `nearby_transit`,
                        `nearby_rail`,
                        `nearby_shopping`,
                        `nearby_parks`,
                        `nearby_airports`,
                        `virtual_tour_urls`,
                        `neighborhood_comments`,
                        `utilities_included`,
                        `expences`,
                        `builiding_office_hours`,
                        `last_update`
                    ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                    $query->execute([
                        $address,
                        $type,
                        $addr1,
                        $addr2,
                        $buildingName,
                        $contactPhone,
                        $latitude,
                        $longitude,
                        $lastUpdated,
                        $petPolicy,
                        $propertyInfo,
                        $outdoorSpace,
                        $services,
                        $studentFeatures,
                        $features,
                        $livingSpace,
                        $kitchen,
                        $security,
                        $contactPerson,
                        $buildingDesc,
                        $walkScore,
                        $transitScore,
                        $leaseLength,
                        $uri,
                        $city,
                        $zip5Code,
                        $state,
                        $imagesLink,
                        $nearbyColleges,
                        $nearbyTransit,
                        $nearbyRail,
                        $nearbyShopping,
                        $nearbyParks,
                        $nearbyAirports,
                        $tours,
                        $neighborhoodComments,
                        $utilsIncluded,
                        $expences,
                        $officeHours,
                        $date
                    ]);

                    // paste avialability
                    $query = $db->pdo->prepare("SELECT `id` FROM `properties` WHERE `link` = ? LIMIT 1");
                    $query->execute([$uri]);
                    $prop = $query->fetch();

                    if ($prop->id) {
                        $propId = $prop->id;
                        foreach ($availability as $item) {
                            $query = $db->pdo->prepare("INSERT INTO `availability` (
                                `property_id`,
                                `bedroom_cnt`,
                                `bathroom_cnt`,
                                `listing_price`,
                                `home_size_sq_ft`,
                                `lease_length`,
                                `status`
                            ) VALUES (?, ?, ?, ?, ?, ?, ?)");
                            $query->execute([
                                $propId,
                                $item['bedroom_cnt'],
                                $item['bathroom_cnt'],
                                $item['listing_price'],
                                $item['home_size_sq_ft'],
                                $item['lease_length'],
                                $item['status']
                            ]);
                        }
                    }
                }
            } elseif ($method === 'update') {
                $db = new MySQL;
                $date = date('Y-m-d');
                $query = $db->pdo->prepare("UPDATE `properties` SET `last_update` = ?, is_deleted = ? WHERE `link` = ?");
                $query->execute([$date, 0 , $uri]);
            }
        } else {
            if ($method === 'update') {
                $db = new MySQL;
                $date = date('Y-m-d');
                $query = $db->pdo->prepare("UPDATE `properties` SET `last_update` = ?, is_deleted = ? WHERE `link` = ?");
                $query->execute([$date, 1, $uri]);
            } else {
                $task = '{"link":"'.$uri.'", "method":"'.$method.'"}';
                Redis::init()->rpush('tasks', $task);
            }
        }
    }

    /**
     * Add log for parsing process.
     *
     * @param Exception|JsonException $e
     */
    protected static function log($e): void
    {
        $message = '['.date('Y-m-d H:i:s').'] Msg: '.$e->getMessage()."\n";
        $message .= 'File: '.$e->getFile().'; Line: '.$e->getLine()."\n";

        file_put_contents(LOG_DIR.'/parse-log.out', $message, FILE_APPEND);
    }

    protected function clearText($text)
    {
        $text = mb_convert_encoding($text, 'cp1251', 'auto');
        $text = preg_replace('/(?:&nbsp;|\h)+/u', ' ', $text);
        $text = preg_replace('/\h*(\R)\s*/u', '$1', $text);
        $text = trim($text);

        return $text;
    }

    protected function getNearbyData($elem, $timeType)
    {
        $nearbyData= [];
        foreach ($elem as $nrb) {
            $nrb = $nrb->find("td");
            if (count($nrb) >= 3) {
                $name = $this->clearText($nrb[0]->text());
                $time = $this->clearText($nrb[1]->text());
                $distance = $this->clearText($nrb[2]->text());
                $nearbyData[] = [
                    'name' => $name,
                    $timeType => $time,
                    'distance' => $distance
                ];
            }
        }
        return !empty($nearbyData) ? json_encode($nearbyData, JSON_PRETTY_PRINT) : '';
    }
}
